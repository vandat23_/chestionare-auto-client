//
//  UIViewController+Notification.m
//  aiCarnet
//
//  Created by Ena on 05/04/14.
//  Copyright (c) 2014 Ena. All rights reserved.
//

#import "UIViewController+Custom.h"
#import <CSNotificationView.h>

@implementation UIViewController (Custom)

- (void) showError:(NSString *) message
{
    [CSNotificationView showInViewController:self
                                       style:CSNotificationViewStyleError
                                     message:message];
}

- (void) showSuccess:(NSString *) message
{
    [CSNotificationView showInViewController:self
                                       style:CSNotificationViewStyleSuccess
                                     message:message];
}
@end

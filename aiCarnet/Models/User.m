//
//  User.m
//  aiCarnet
//
//  Created by Ena on 14/03/14.
//  Copyright (c) 2014 Ena. All rights reserved.
//

#import "User.h"

@implementation User
-(User *)init{
    return [super init];
}

-(User *)initWithEmail:(NSString *)email andWithPassword:(NSString *)password{
    self = [super init];
    if (self) {
        self.email = email;
        self.password = password;
    }
    return self;
    
}

-(User *)initFromMemory{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *email = [defaults objectForKey:@"email"];
    NSString *password = [defaults objectForKey:@"password"];
    
    if([email length] == 0  || [password length] == 0){
        return [self init];
    }
    
    return [self initWithEmail:email andWithPassword:password];
}

-(void)saveToMemory{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:self.email forKey:@"email"];
    [defaults setObject:self.password forKey:@"password"];

    [defaults synchronize];
    
    NSLog(@"Data saved");
}

-(BOOL) isValid{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return  [self.password length] > 0 && [emailTest evaluateWithObject:self.email];
}

-(NSString *)serialize{
    return [NSString stringWithFormat:@"?username=%@&password=%@", self.email,self.password];
}


@end

//
//  CheckboxQuestionView.h
//  QuestionaireApp
//
//  Created by Ena on 26/12/13.
//  Copyright (c) 2013 Ena. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Question.h"

@interface QuestionView : UIView
@property Question *question;

- (void) draw:(Question *)question;
- (void) showAnswers;
- (NSMutableArray *) getAnswers;
@end

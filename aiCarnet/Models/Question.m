//
//  Question.m
//  QuestionaireApp
//
//  Created by Ena on 26/12/13.
//  Copyright (c) 2013 Ena. All rights reserved.
//

#import "Question.h"
#import "Answer.h"

@implementation Question

- initWithRequestObject:(id) object
{
    if (self = [super init]) {
        [self setQuestion:[object objectForKey:@"text"]];
        
        if (![[object objectForKey:@"poza"] isEqualToString:@"none"]) {
            [self setImage:[object objectForKey:@"poza"]];
        }
        self.answers = [[NSMutableArray alloc] init];
        for (id answerObject in [object objectForKey:@"answers"]) {
            Answer *answer = [[Answer alloc] initWithRequestObject:answerObject];
            [self.answers addObject:answer];
        }
        self.userAnswers = [[NSMutableArray alloc] init];
    }
    return self;
}
- (NSString *) serializeAnswers{
    NSString *result = @"";
    for(int i=0; i< [self.answers count]; i++){
        if ([[self.answers objectAtIndex:i] isCorrect]) {
            result = [NSString stringWithFormat:@"%@%d", result, i];
        }
    }
    return result;
}

- (BOOL) isCorrectAnswered{
    return  [[self serializeAnswers] isEqual:[self.userAnswers componentsJoinedByString:@""]];
}

@end

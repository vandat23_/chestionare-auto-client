//
//  RegisterViewController.h
//  aiCarnet
//
//  Created by Ena on 11/03/14.
//  Copyright (c) 2014 Ena. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UITextField *emailTextView;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextView;
@end

//
//  RegisterViewController.m
//  aiCarnet
//
//  Created by Ena on 11/03/14.
//  Copyright (c) 2014 Ena. All rights reserved.
//

#import "RegisterViewController.h"
#import <CSNotificationView.h>
#import "User.h"

@interface RegisterViewController ()

@end

@implementation RegisterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.titleLabel setFont:[UIFont fontWithName:@"RobotoCondensed-Regular" size:50]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)touchClose:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)touchSignUp:(id)sender {
    User *user = [[User alloc] init];
    [user setPassword: [self.passwordTextView text]];
    [user setEmail:[self.emailTextView text]];
    
    if([user isValid]){
        CSNotificationView *loadingNotification = [CSNotificationView notificationViewWithParentViewController:self tintColor:[UIColor orangeColor] image:nil message:@"Loading..."];
        
        [loadingNotification setShowingActivity:YES];
        [loadingNotification setVisible:YES animated:YES completion:nil];

        Request *request = [[Request alloc] init];
        [request POST:[NSString stringWithFormat:@"register/%@",[user serialize]]
           parameters:nil
              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                  [loadingNotification setVisible:NO animated:YES completion:nil];
                  if ([[responseObject objectForKey:@"status"] isEqualToString:@"success"]) {
                      [user saveToMemory];
                      [UIView transitionWithView:[[[UIApplication sharedApplication] delegate] window]
                                        duration:0.5
                                         options:(UIViewAnimationOptionTransitionFlipFromLeft | UIViewAnimationOptionAllowAnimatedContent)
                                      animations:^{
                                          UINavigationController *mainController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"main"];
                                          [[[[UIApplication sharedApplication] delegate] window] setRootViewController:mainController];
                                      }
                                      completion:nil];
                  }else{
                      [self showError:[responseObject objectForKey:@"data"]];
                  }
              }
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  [loadingNotification setVisible:NO animated:YES completion:nil];
                  [self showError:[error description]];
              }];
    }else{
        [self showError:@"Email or password are required please try again!"];
    }
}
@end

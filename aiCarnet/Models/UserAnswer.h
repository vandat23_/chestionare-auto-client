//
//  UserAnswer.h
//  QuestionaireApp
//
//  Created by Ena on 10/01/14.
//  Copyright (c) 2014 Ena. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserAnswer : NSObject
@property NSInteger questionnaireID;
@property NSInteger questionID;
@property NSString *answer;
@end

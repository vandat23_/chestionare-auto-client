//
//  HomViewController.m
//  aiCarnet
//
//  Created by Ena on 10/03/14.
//  Copyright (c) 2014 Ena. All rights reserved.
//

#import "HomeViewController.h"
#import "QuestionnaireViewController.h"
#import "StatisticsViewController.h"
#import "Questionnaire.h"
#import "User.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)touchExam:(id)sender {
    [self goToQuestionnaire:@"exam"];
}

- (IBAction)touchPractice:(id)sender {
    [self goToQuestionnaire:@"practice"];
}

-(void) goToQuestionnaire:(NSString *)mode{
    int randNum = arc4random_uniform(30) + 1; //create the random number between 0 and 30.
    Request *request = [[Request alloc] init];
    [request GET:[NSString stringWithFormat:@"questionnaire/%d", randNum]
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             if ([[responseObject objectForKey:@"status"] isEqualToString:@"success"]) {
                 Questionnaire *questionnaire = [[Questionnaire alloc] initWithRequestObject:[responseObject objectForKey:@"data"]];
                 [questionnaire setQuestionnaireID:randNum];
                 
                 QuestionnaireViewController *questionnaireViewController = (QuestionnaireViewController *)[[UIStoryboard storyboardWithName:@"Main" bundle: nil] instantiateViewControllerWithIdentifier: @"questionnaire"];
                 [questionnaireViewController setMode:mode];
                 [questionnaireViewController setQuestionnaire:questionnaire];
                 
                 [self.navigationController pushViewController:questionnaireViewController animated:YES];
             }else{
                 [self showError:[responseObject objectForKey:@"data"]];
             }
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             [self showError:[error description]];
         }];
}

- (IBAction)touchStats:(id)sender {
    User *user = [[User alloc] initFromMemory];
    Request *request = [[Request alloc] init];
    [request GET:[NSString stringWithFormat:@"stats/%@", [user email]]
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             if ([[responseObject objectForKey:@"status"] isEqualToString:@"success"]) {
                 StatisticsViewController *statisticsViewController = (StatisticsViewController *)[[UIStoryboard storyboardWithName:@"Main" bundle: nil] instantiateViewControllerWithIdentifier: @"statistics"];
                 [statisticsViewController setStats:[responseObject objectForKey:@"data"]];
                 [self.navigationController pushViewController:statisticsViewController animated:YES];
             }else{
                 [self showError:[responseObject objectForKey:@"data"]];
             }
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             [self showError:[error description]];
         }];
}

- (IBAction)touchSettings:(id)sender {
//    [self performSegueWithIdentifier:@"register" sender:self];
}
@end

//
//  UIViewController+Notification.h
//  aiCarnet
//
//  Created by Ena on 05/04/14.
//  Copyright (c) 2014 Ena. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Custom)

//Notifications
- (void) showError:(NSString *) message;
- (void) showSuccess:(NSString *) message;

@end

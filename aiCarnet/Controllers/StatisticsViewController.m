//
//  StatisticsViewController.m
//  aiCarnet
//
//  Created by Ena on 01/04/14.
//  Copyright (c) 2014 Ena. All rights reserved.
//

#import "StatisticsViewController.h"
#import "User.h"

@interface StatisticsViewController ()

@end

@implementation StatisticsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self drawLineChart];
    [self drawBarChart];
    [self drawPieChart];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)drawLineChart
{
    PNLineChart * lineChart = [[PNLineChart alloc] initWithFrame:CGRectMake(0, 25, self.todayProgressView.frame.size.width, self.todayProgressView.frame.size.height - 25)];
    [lineChart setBackgroundColor:[UIColor clearColor]];
    
    NSArray *data = [self.stats objectForKey:@"today"];
    NSMutableArray *lineChartLabels = [[NSMutableArray alloc] init];
    for (int i=0; i<[data count]; i++) {
        [lineChartLabels addObject:[NSString stringWithFormat:@"Ch. %@", @(i)]];
    }
    
    // set labels
    [lineChart setXLabels:lineChartLabels];
    
    // set values
    PNLineChartData *lineChartData = [PNLineChartData new];
    lineChartData.color = PNCloudWhite;
    lineChartData.itemCount = lineChart.xLabels.count;
    lineChartData.getData = ^(NSUInteger index) {
        CGFloat yValue = [[data objectAtIndex:index] floatValue];
        return [PNLineChartDataItem dataItemWithY:yValue];
    };

    lineChart.chartData = @[lineChartData];
    [lineChart strokeChart];
    
    [self.todayProgressView addSubview:lineChart];
}

- (void)drawBarChart
{
    id data = [self.stats objectForKey:@"lastWeek"];
    
    PNBarChart * barChart = [[PNBarChart alloc] initWithFrame:CGRectMake(0, 20, self.lastWeekProgressView.frame.size.width, self.lastWeekProgressView.frame.size.height - 5)];
    
    NSMutableArray *labels = [[NSMutableArray alloc] init];
    NSMutableArray *values = [[NSMutableArray alloc] init];
    for (id object in data) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        
        NSDate *date = [dateFormatter dateFromString:[object objectForKey:@"date"]];
        
        [dateFormatter setDateFormat:@"EEE"];
        [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"ro_RO"]];
        [labels addObject:[dateFormatter stringFromDate:date]];
        
        NSInteger total = [[object objectForKey:@"total"] integerValue];
        NSInteger passed = [[object objectForKey:@"passed"] integerValue];
        
        NSInteger procent = 0;
        if (total != 0) {
            procent = passed / total * 100;
        }
        [values addObject:@(procent)];
    }
    
    [barChart setXLabels:labels];
    [barChart setYValues:values];
    [barChart strokeChart];
    
    [self.lastWeekProgressView addSubview:barChart];
}

- (void)drawPieChart
{
    id data = [self.stats objectForKey:@"global"];
    
    PNCircleChart * circleChart = [[PNCircleChart alloc] initWithFrame:CGRectMake(0, 5, self.globalProgressView.frame.size.width - 200, self.globalProgressView.frame.size.height - 20) andTotal:[data objectForKey:@"passed"] andCurrent:[data objectForKey:@"total"]];
    
    [circleChart setBackgroundColor:[UIColor clearColor]];
    [circleChart setStrokeColor:PNGreen];
    [circleChart strokeChart];
    
    [self.globalProgressView addSubview:circleChart];
}


@end

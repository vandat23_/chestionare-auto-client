//
//  Questionaire.h
//  QuestionaireApp
//
//  Created by Ena on 22/12/13.
//  Copyright (c) 2013 Ena. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Questionnaire : NSObject
@property NSInteger questionnaireID;
@property NSMutableArray *questions;

- initWithRequestObject:(id) object;
@end

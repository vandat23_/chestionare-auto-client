//
//  StatisticsViewController.h
//  aiCarnet
//
//  Created by Ena on 01/04/14.
//  Copyright (c) 2014 Ena. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PNChart.h>

@interface StatisticsViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIView *todayProgressView;
@property (strong, nonatomic) IBOutlet UIView  *lastWeekProgressView;
@property (strong, nonatomic) IBOutlet UIView *globalProgressView;

@property (strong, nonatomic) id stats;
@end

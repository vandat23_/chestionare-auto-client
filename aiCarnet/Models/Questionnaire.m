//
//  Questionaire.m
//  QuestionaireApp
//
//  Created by Ena on 22/12/13.
//  Copyright (c) 2013 Ena. All rights reserved.
//

#import "Questionnaire.h"
#import "Question.h"

@implementation Questionnaire

- initWithRequestObject:(id)object
{
    if (self = [super init]) {
        self.questions = [[NSMutableArray alloc] init];
        for (id questionObject in [object objectForKey:@"questions"]) {
            Question *question = [[Question alloc] initWithRequestObject:questionObject];
            [self.questions addObject:question];
        }
    }
    return self;
}
@end




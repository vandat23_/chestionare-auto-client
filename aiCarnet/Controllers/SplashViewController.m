//
//  SplashViewController.m
//  aiCarnet
//
//  Created by Ena on 14/03/14.
//  Copyright (c) 2014 Ena. All rights reserved.
//

#import "SplashViewController.h"
#import "User.h"

@interface SplashViewController ()

@end

@implementation SplashViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    User *user = [[User alloc] initFromMemory];
    if([user.email length] == 0 || [user.password length] == 0){
        [self performSegueWithIdentifier:@"login" sender:self];
    }else{
        Request *request = [[Request alloc] init];
        [request POST:[NSString stringWithFormat:@"login/%@",[user serialize]]
           parameters:nil
              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                  if ([[responseObject objectForKey:@"status"] isEqualToString:@"success"]) {
                      [UIView transitionWithView:[[[UIApplication sharedApplication] delegate] window]
                                        duration:0.5
                                         options:(UIViewAnimationOptionTransitionFlipFromLeft | UIViewAnimationOptionAllowAnimatedContent)
                                      animations:^{
                                          UINavigationController *mainController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"main"];
                                          [[[[UIApplication sharedApplication] delegate] window] setRootViewController:mainController];
                                      }
                                      completion:nil];
                  }else{
                      [self performSegueWithIdentifier:@"login" sender:self];
                  }
              }
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  [self performSegueWithIdentifier:@"login" sender:self];
              }];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

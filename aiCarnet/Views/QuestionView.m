//
//  CheckboxQuestionView.m
//  QuestionaireApp
//
//  Created by Ena on 26/12/13.
//  Copyright (c) 2013 Ena. All rights reserved.
//

#import "QuestionView.h"
//#import "QuestionLabel.h"
#import "Answer.h"

@implementation QuestionView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)draw:(Question *)question
{
    
    [self setQuestion:question];
//    [self setBackgroundColor:[UIColor redColor]];
    
    NSInteger y = 0, height = 120, tag=1;
    
    if ([[question image] length] > 0) {
        NSURL *url = [NSURL URLWithString:[question image]];
        UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:url]];
        
        UIImageView *questionImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, y, 310, 136)];
        [questionImageView setContentMode:UIViewContentModeScaleAspectFit];
        [questionImageView setImage:image];
        [questionImageView setBackgroundColor:[UIColor whiteColor]];
        
        [self addSubview:questionImageView];
        
        y = 136;
        height = 55;
    }
    
    
    UILabel *questionLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, y, 310, height)];
    [questionLabel setText:[question question]];
    [questionLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:16]];
    [questionLabel setNumberOfLines:5];
    [questionLabel setTextAlignment:NSTextAlignmentCenter];
    [questionLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [questionLabel setBackgroundColor:[UIColor whiteColor]];
    [self addSubview:questionLabel];
    
    y += height + 4;
    
    for (Answer *answer in question.answers)
    {
        UIButton *answerButton = [[UIButton alloc] initWithFrame:CGRectMake(0, y, 310, 65)];
        [answerButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:14]];
        [answerButton.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
        [answerButton.titleLabel setTextAlignment:NSTextAlignmentLeft];
        
        [answerButton setBackgroundColor:[UIColor whiteColor]];
        [answerButton setContentEdgeInsets:UIEdgeInsetsMake(5, 5, 5, 5)];
        [answerButton setTag:tag];tag++;
        
//        Normal
        [answerButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [answerButton setTitle:[answer answer] forState:UIControlStateNormal];
        
//        Selected
        [answerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        
//        Disabled
//        [answerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateDisabled];
        
        [answerButton addTarget:self action:@selector(touchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        [answerButton setUserInteractionEnabled:YES];

        [self addSubview:answerButton];
        y += 69;
    }
//    [self showAnswers];
}

- (void) touchUpInside:(id) sender
{
    if ([sender isSelected]) {
        [sender setBackgroundColor:[UIColor whiteColor]];
    }else{
        [sender setBackgroundColor:[UIColor colorWithRGB:0x323a45]];
    }
    
    [sender setSelected:![sender isSelected]];
}

- (NSMutableArray *) getAnswers
{
    NSMutableArray *results = [[NSMutableArray alloc] init];
    for (id view in [self subviews]) {
        if ([view isKindOfClass:[UIButton class]]) {
            if ([view isSelected]) {
                [results addObject:@(([view tag] - 1))];
            }
        }
    }
    return results;
}

- (void) showAnswers
{
    if ([[self.question userAnswers] count] == 0) {
        return;
    }
    
    NSInteger index;
    for (id view in [self subviews]) {
        if ([view isKindOfClass:[UIButton class]]) {
            index = [view tag] - 1;
            
            if ([[[self.question answers] objectAtIndex:index] isCorrect]) {
                [view setBackgroundColor:[UIColor colorWithRGB:0x38a896]];
                [view setTitleColor:[UIColor whiteColor] forState:UIControlStateDisabled];
            }
            
            if([[[self.question answers] objectAtIndex:index] isCorrect] != [[self.question userAnswers] containsObject:@(index)] &&
               ![[[self.question answers] objectAtIndex:index] isCorrect]) {
                [view setBackgroundColor:[UIColor colorWithRGB:0xbf2d2e]];
                [view setTitleColor:[UIColor whiteColor] forState:UIControlStateDisabled];
            }
            
            if ([[self.question userAnswers] containsObject:@(index)]) {
                [((UIButton *)view).titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:14]];
            }
            
            [view setEnabled:NO];
        }
        
    }
}
@end

//
//  Answer.m
//  QuestionaireApp
//
//  Created by Ena on 26/12/13.
//  Copyright (c) 2013 Ena. All rights reserved.
//

#import "Answer.h"

@implementation Answer
- initWithRequestObject:(id) object
{
    if (self = [super init]) {
        [self setAnswer:[object objectForKey:@"text"]];
        [self setIsCorrect:[[object objectForKey:@"is_correct"] isEqualToNumber:@(1)]];
    }
    return self;
}

@end

//
//  Answer.h
//  QuestionaireApp
//
//  Created by Ena on 26/12/13.
//  Copyright (c) 2013 Ena. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Answer : NSObject
@property NSString *answer;
@property BOOL isCorrect;

- initWithRequestObject:(id) object;

@end

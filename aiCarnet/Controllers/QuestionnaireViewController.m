//
//  QuestionaireViewController.m
//  QuestionaireApp
//
//  Created by Ena on 09/01/14.
//  Copyright (c) 2014 Ena. All rights reserved.
//

#import "QuestionnaireViewController.h"
#import <CSNotificationView.h>
#import <SIAlertView/SIAlertView.h>
#import "Question.h"
#import "Answer.h"
#import "User.h"

@interface QuestionnaireViewController ()

@end

@implementation QuestionnaireViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.currentQuestionIndex = 0;
    self.correntAnswers = 0;
    self.wrongAnswers = 0;
    
    self.timer = [self.timer initWithTimerType:MZTimerLabelTypeTimer];
    [self.timer setBackgroundColor:[UIColor colorWithRGB:0xbf2d2e]];
    [self.timer setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:32]];
    [self.timer setTextColor:[UIColor whiteColor]];
    [self.timer setCountDownTime:1800];
    [self.timer setTimeFormat:@"mm:ss"];
    [self.timer startWithEndingBlock:^(NSTimeInterval countTime) {
        if ([self.mode isEqualToString:@"exam"]) {
            [self sendAnswers];
        }
    }];
    
    [self drawQuestion];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)drawQuestion
{
    [UIView beginAnimations:NULL context:Nil];
    {
        [UIView setAnimationDuration:0.3];
        
        if ([self currentView] != Nil) {
            [[self currentView] removeFromSuperview];
        }
        
        [self setTitle:[NSString stringWithFormat:@"Intrebarea %ld", (long)self.currentQuestionIndex + 1]];
    
        Question *question = [[[self questionnaire] questions] objectAtIndex:self.currentQuestionIndex];

        QuestionView *questionView = [[QuestionView alloc] initWithFrame:CGRectMake(5, 53, 310, 398)];
        [questionView draw:question];
        
        [self.answerButton setEnabled:YES];
        if ([[question userAnswers] count] > 0) {
            [self.answerButton setEnabled:NO];
        }
        
        [self setCurrentView:questionView];
        [self.view addSubview:questionView];
    }
    [UIView commitAnimations];
    [self.currentView showAnswers];
}

- (IBAction)onNextTouch:(id)sender
{
    NSInteger next = self.currentQuestionIndex + 1;
    if(next == [[[self questionnaire] questions] count]){
        next = 0;
    }

    [self setCurrentQuestionIndex:next];
    [self drawQuestion];
}

- (IBAction)onPrevTouch:(id)sender
{
    NSInteger next = self.currentQuestionIndex - 1;
    
    if(next == -1){
        next = [[[self questionnaire] questions] count] - 1;
    }
    
    [self setCurrentQuestionIndex:next];
    [self drawQuestion];
}

- (IBAction)onAnswerTouch:(id)sender
{
    Question *question = [[[self questionnaire] questions] objectAtIndex:self.currentQuestionIndex];
    
    NSMutableArray *userAnswers = [self.currentView getAnswers];
    if ([userAnswers count] == 0) {
        return;
    }
    
    [question setUserAnswers:userAnswers];
    if ([question isCorrectAnswered]) {
        self.correntAnswers++;
        [self.correntAnswersLable setText:[NSString stringWithFormat:@"%ld", (long)self.correntAnswers]];
    }else{
        self.wrongAnswers++;
        [self.wrongAnswersLable setText:[NSString stringWithFormat:@"%ld", (long)self.wrongAnswers]];
    }
    
    if ([self.mode isEqualToString:@"practice"]) {
        [self.currentView showAnswers];
    }else{
        [[self.questionnaire questions] removeObject:question];
    }
   
    if(self.correntAnswers + self.wrongAnswers < [[self.questionnaire questions] count] &&
       (self.wrongAnswers < 5 && [self.mode isEqualToString:@"exam"])){
        [NSTimer scheduledTimerWithTimeInterval:.5
                                         target:self
                                       selector:@selector(onNextTouch:)
                                       userInfo:nil
                                        repeats:NO];
    }
    else {
        [self sendAnswers];
    }
}

-(void) sendAnswers{
    CSNotificationView *loadingNotification = [CSNotificationView notificationViewWithParentViewController:self tintColor:[UIColor orangeColor] image:nil message:@"Saving results..."];
    [loadingNotification setShowingActivity:YES];
    [loadingNotification setVisible:YES animated:YES completion:nil];
    
    NSString *title,*message;
    
    if (self.correntAnswers >= 22) {
        title   = @"Admis";
        message = [NSString stringWithFormat:@"Ai obitnut %ld puncte", (long)self.correntAnswers];
    }else{
        title =  @"Respins";
        message = [NSString stringWithFormat:@"Ai obitnut %ld puncte. Iti trebuie minim 22 de puncte.", (long)self.correntAnswers];
    }
    
    User *user = [[User alloc] initFromMemory];
    Request *request = [[Request alloc] init];
    [request POST:[NSString stringWithFormat:@"questionnaire/%@/%ld/%ld", [user email], (long)self.correntAnswers, (long)[self.questionnaire questionnaireID]]
       parameters:nil
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              [loadingNotification setVisible:NO animated:YES completion:nil];
              if ([[responseObject objectForKey:@"status"] isEqualToString:@"success"]) {
                  SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:title andMessage:message];
                  
                  [alertView addButtonWithTitle:@"Ok"
                                           type:SIAlertViewButtonTypeDestructive
                                        handler:^(SIAlertView *alert) {
                                            [self.navigationController popViewControllerAnimated:YES];
                                        }];
                  alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
                  
                  [alertView show];
              }else{
                  [self showError:[responseObject objectForKey:@"data"]];
              }
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              [loadingNotification setVisible:NO animated:YES completion:nil];
              [self showError:[error description]];
          }];
}

@end

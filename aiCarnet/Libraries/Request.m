//
//  Rest.m
//  aiCarnet
//
//  Created by Ena on 14/03/14.
//  Copyright (c) 2014 Ena. All rights reserved.
//

#import "Request.h"

@implementation Request

-(Request *)init{
    self = [super initWithBaseURL:[NSURL URLWithString:SERVICE_URL]];
    if (self) {
        [[self responseSerializer] setAcceptableContentTypes:[NSSet setWithObject:@"application/json"]];
        [self setRequestSerializer:[AFJSONRequestSerializer serializer]];
        [self setResponseSerializer:[AFJSONResponseSerializer serializer]];
    }
    return self;
}
@end

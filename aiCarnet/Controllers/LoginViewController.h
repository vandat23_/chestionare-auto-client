//
//  LoginViewController.h
//  aiCarnet
//
//  Created by Ena on 07/03/14.
//  Copyright (c) 2014 Ena. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UITextField *emailTextView;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextView;
@end

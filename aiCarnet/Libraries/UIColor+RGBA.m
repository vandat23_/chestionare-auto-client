//
//  UIColor+RGBA.m
//  aiCarnet
//
//  Created by Ena on 14/03/14.
//  Copyright (c) 2014 Ena. All rights reserved.
//

#import "UIColor+RGBA.h"

@implementation UIColor (RGBA)
+(UIColor *)colorWithRGB:(NSUInteger)color{
    return [UIColor colorWithRed:((float)((color & 0xFF0000) >> 16))/255.0
                           green:((float)((color & 0xFF00) >> 8))/255.0
                            blue:((float)(color & 0xFF))/255.0 alpha:1.0];
}
@end

//
//  UIColor+RGBA.h
//  aiCarnet
//
//  Created by Ena on 14/03/14.
//  Copyright (c) 2014 Ena. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (RGBA)
+(UIColor *)colorWithRGB:(NSUInteger)color;
@end

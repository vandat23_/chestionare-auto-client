//
//  User.h
//  aiCarnet
//
//  Created by Ena on 14/03/14.
//  Copyright (c) 2014 Ena. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject
@property NSString *email;
@property NSString *password;

-(User *)init;
-(User *)initWithEmail:(NSString *)email andWithPassword:(NSString *)password;
-(User *)initFromMemory;

-(NSDictionary *)serialize;

-(void)saveToMemory;

-(BOOL) isValid;
@end

//
//  UserTestCase.m
//  aiCarnet
//
//  Created by Ena on 14/03/14.
//  Copyright (c) 2014 Ena. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "User.h"
@interface UserTestCase : XCTestCase

@end

@implementation UserTestCase

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testValidUser
{
    User *user = [[User alloc] init];
    [user setEmail:@"elena.roman@hype.ro"];
    [user setPassword:@"parola"];
    
    XCTAssertTrue([user isValid], @"Should be a valid user");
}

- (void)testValidUserMissingPassword
{
    User *user = [[User alloc] init];
    [user setEmail:@"elena.roman@hype.ro"];
    [user setPassword:@""];
    
    XCTAssertTrue(![user isValid], @"Should be an invalid user");
}

- (void)testValidUserMissingEmail
{
    User *user = [[User alloc] init];
    [user setEmail:@""];
    [user setPassword:@"parola"];
    
    XCTAssertTrue(![user isValid], @"Should be an invalid user");
}

- (void)testValidUserMissingEmailAndPassword
{
    User *user = [[User alloc] init];
    [user setEmail:@""];
    [user setPassword:@""];
    
    XCTAssertTrue(![user isValid], @"Should be an invalid user");
}

- (void)testValidUserMissingInvalidEmail
{
    User *user = [[User alloc] init];
    [user setEmail:@"a"];
    [user setPassword:@"parola"];
    
    XCTAssertTrue(![user isValid], @"Should be an invalid user");
}
@end

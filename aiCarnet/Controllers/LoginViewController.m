//
//  LoginViewController.m
//  aiCarnet
//
//  Created by Ena on 07/03/14.
//  Copyright (c) 2014 Ena. All rights reserved.
//

#import "LoginViewController.h"
#import "User.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.titleLabel setFont:[UIFont fontWithName:@"RobotoCondensed-Regular" size:50]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)touchSignUp:(id)sender {
    [self performSegueWithIdentifier:@"register" sender:self];
}

- (IBAction)touchSignIn:(id)sender {
    User *user = [[User alloc] init];
    [user setPassword: [self.passwordTextView text]];
    [user setEmail:[self.emailTextView text]];
    
    Request *request = [[Request alloc] init];
    [request POST:[NSString stringWithFormat:@"login/%@",[user serialize]]
       parameters:nil
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              if ([[responseObject objectForKey:@"status"] isEqualToString:@"success"]) {
                  [user saveToMemory];
                  [UIView transitionWithView:[[[UIApplication sharedApplication] delegate] window]
                                    duration:0.5
                                     options:(UIViewAnimationOptionTransitionFlipFromLeft | UIViewAnimationOptionAllowAnimatedContent)
                                  animations:^{
                                      UINavigationController *mainController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"main"];
                                      [[[[UIApplication sharedApplication] delegate] window] setRootViewController:mainController];
                                  }
                                  completion:nil];
              }else{
                  [self showError:[responseObject objectForKey:@"data"]];
              }
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              [self showError:[error description]];
          }];
}
@end

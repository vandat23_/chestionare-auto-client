//
//  Question.h
//  QuestionaireApp
//
//  Created by Ena on 26/12/13.
//  Copyright (c) 2013 Ena. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Question : NSObject
@property NSString *question;
@property NSString *image;
@property NSMutableArray *answers;
@property NSMutableArray *userAnswers;

- initWithRequestObject:(id) object;
- (NSString *) getAnswers;
- (BOOL) isCorrectAnswered;
@end

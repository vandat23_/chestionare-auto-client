//
//  QuestionaireViewController.h
//  QuestionaireApp
//
//  Created by Ena on 09/01/14.
//  Copyright (c) 2014 Ena. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Questionnaire.h"
#import "QuestionView.h"
#import "MZTimerLabel.h"

@interface QuestionnaireViewController : UIViewController
@property (strong, nonatomic) NSString *mode;
@property (strong, nonatomic) Questionnaire *questionnaire;
@property (strong, nonatomic) IBOutlet UIButton *answerButton;

@property (strong, nonatomic) IBOutlet MZTimerLabel *timer;

@property (strong, nonatomic) IBOutlet UILabel *correntAnswersLable;
@property (strong, nonatomic) IBOutlet UILabel *wrongAnswersLable;
@property NSInteger correntAnswers;
@property NSInteger wrongAnswers;

@property (strong, nonatomic) QuestionView *currentView;
@property NSInteger currentQuestionIndex;


@end
